!SLIDE
#Other Technologies Used
![logo](logo.png) ![sass](sass.jpg) ![zepto](zepto_js.jpg)

!SLIDE bullets incremental

##Other Technologies cont...

* Coffescript: scripting language that compiles to Javascript
* Sass: Synaptically Awesome Styesheets
* Zepto: jQuery for mobile. Smaller, less in it, more touch events (pinch, swipe...)

!SLIDE bullets incremental
#Coffeescript

* "It's just JavaScript"
* compiles to good javascript
* still think JavaScript
* writes a little nicer
* gives some cool features

!SLIDE execute smaller

Coffeescript:

    @@@ Coffeescript
    sampleObject =
      x: 1
      y: 3

    sumXY = (xyObj) ->
      xyObj.x + xyObj.y

    alert sumXY(sampleObject) if sampleObject.x?

Javascript:

    @@@ Javascript
    var sampleObject, sumXY;

    sampleObject = {
      x: 1,
      y: 3
    };

    sumXY = function(xyObj) {
      return xyObj.x + xyObj.y;
    };

    if (sampleObject.x != null) alert(sumXY(sampleObject));

!SLIDE code small

###Coffeescript:

    @@@ Coffeescript
    books = [
      {display: "Genesis", chapters: 50, osis: "Gen"}
      {display: "Exodus", chapters: 40,  osis: "Exod"}
    ]

    bookList = for book in books
      {title: book.display}

!SLIDE code smaller

    @@@ Javascript
    var book, bookList, books;

    books = [
      {
        display: "Genesis",
        chapters: 50,
        osis: "Gen"
      }, {
        display: "Exodus",
        chapters: 40,
        osis: "Exod"
      }
    ];

    bookList = (function() {
      var _i, _len, _results;
      _results = [];
      for (_i = 0, _len = books.length; _i < _len; _i++) {
        book = books[_i];
        _results.push({
          title: book.display
        });
      }
      return _results;
    })();

!SLIDE bullets incremental

#Opinions after

* It's nice
* It's not neccisary
* It's readable and writes very readable JS
* Doesn't replace JS, just makes it nicer
* Encourages pretty code