!SLIDE center
# Appcelerator Titanium #
![site](site.png)

!SLIDE subsection
# Code: Background Info

!SLIDE bullets incremental
# What is it? #

* Javascript framework for building native mobile apps
* IDE based on eclipse
* builds iOS, Android, and web*

!SLIDE CENTER
# Used by:
![customers](customers.png)

!SLIDE center
# Mobile Marketplace
![store](store.png)

.notes minifies and base64 encodes js. stored as a variable in a c file. uses device JavaScriptCore interpreter to durring runtime. js doesn't run in browser, unless you are using a webview and js with it

!SLIDE center
# The IDE
![ide](ide.png)

!SLIDE bullets incremental
# What it's not #

* phonegap (webapp in wrapped in a native app)
* write once, run everywhere
* a magic bullet

!SLIDE subsection
# Code: Creating Layout

!SLIDE code smaller

    @@@ JavaScript
    var versePickerWheels = Ti.UI.createView({
      backgroundImage: "images/bg_picker_2col.png",
      top: 71,
      left: 19,
      height: 480,
      width: 245
    });
    var verseSearch = Titanium.UI.createSearchBar({
      height: 44,
      width: 245,
      top: 29,
      left: 19
    });
    var booksView = Ti.UI.createTableView({
        data: bookList,
        backgroundColor: "transparent",
        showVerticalScrollIndicator: false,
        separatorColor: "transparent",
        height: 480,
        top: 74,
        width: 169,
        left: 20,
        allowsSelection: false,
        headerView: paddingView,
        footerView: paddingView
      });

!SLIDE code
    @@@ JavaScript
    var versePicker = Ti.UI.createView({
      width: 278,
      height: 614,
      top: 60,
      left: 50,
    });

    versePicker.add(versePickerWheels);
    versePicker.add(verseSearch);
    versePicker.add(booksView);

!SLIDE subsection
# Code: Handling Events

!SLIDE code smaller
    @@@ JavaScript
    searchButton.addEventListener("click", function(e) {
      var chapter, osis;
      osis = BG.bibleInfo.data[booksView.selected].osis;
      chapter = chaptersView.selected + 1;
      versePicker.hide();
      BG.bible.updatePassage(osis, chapter, null, function() {});
    });

!SLIDE center

.notes the app

![app1](app1.png)

!SLIDE subsection
# Things Learned

!SLIDE bullets incremental
# The Good

* javascript works well
* mature product
* debugging tools
* didn't feel any limitations
* fun to write and worked really well

!SLIDE bullets incremental
# The Bad

* the IDE
* no built in testing
* viewing took time (8-25 secs) to compile (compared to web)

!SLIDE bullets incremental
#The Ugly

* implicit memory management
* dependent on 3rd party platform

!SLIDE bullets incremental
#Conclusion

* works well
* enjoyed using callbacks and JS objects
* not a silver bullet